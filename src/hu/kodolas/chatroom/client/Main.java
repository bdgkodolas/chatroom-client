package hu.kodolas.chatroom.client;

import java.awt.Dimension;
import java.io.IOException;
import javax.swing.JFrame;

public class Main {

    public static void main(String[] args) throws IOException {
        
        JFrame frame = new JFrame("ChatRoom"); // új ablak létrehozása ChatRoom címmel
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // az ablak bezárásánál lépjen ki a program
        frame.setPreferredSize(new Dimension(640, 480)); // az ablak méretének beállítása
        frame.setResizable(false); // ne legyen átméretezhető az ablak a felhasználó által
        
        ChatClient chatClient = new ChatClient();
        GUI gui = new GUI(frame, chatClient);
        frame.add(gui); // a GUI objektum hozzáadása az ablakhoz
        chatClient.gui = gui;

        frame.pack();
        frame.setVisible(true); // legyen látható az ablak
        
        chatClient.getOnlineUsers(); // az online felhasználók lekérdezése
        chatClient.listen(); // a ChatClient várja a szerverről érkező üzeneteket
        
    }

}
