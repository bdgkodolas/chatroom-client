package hu.kodolas.chatroom.client;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonStreamParser;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/*
 * Ez az osztály felel a szerverrel való kommunikációért.
 * A szerver felé küldött üzeneteket és a rájuk érkező
 * választ kezeljük az 'action' csatornán.
 * Az szerver által küldött üzeneteket az 'event' csatornán
 * fogadjuk.

 * Mindkét csatornán egy JSON alapú protokollal (szabvánnyal)
 * történik a kommunikáció.
 *
 * A protokoll a szerver GitLab projektjénél van leírva:
 * https://gitlab.com/techkor/chatroom-server#chatroom-protocol
 *
 * A JSON-ról bővebben:
 * https://www.digitalocean.com/community/tutorials/an-introduction-to-json
 * https://hu.wikipedia.org/wiki/JSON
 */

public class ChatClient {
    
    // Ez a szerver hosztneve. A localhost azt jelenti, hogy a szerver
    // a helyi számítógép.
    private static final String HOSTNAME = "localhost";
    // A felhasználó beceneve. Jozsi után legyen egy random szám 0 és 99 között,
    // pl. Jozsi12
    private static final String NICKNAME = "Jozsi" + new Random().nextInt(100);

    Writer out;                                         // Az action kimenet a szerver felé
    JsonStreamParser actionParser;                      // Az action bemenetet feldolgozó JSON parser. A JsonStreamParser a bemenetből JsonElement-et tud feldolgozni.
    public int id;                                      // A kliens azonosítója. A szervertől kapjuk majd meg.
    private int msgid;                                  // Az üzenet azonosítója
    Map<Integer, String> onlineUsers = new HashMap<>(); // Az online felhasználókat itt tároljuk, kulcs-érték párokban, ahol a kulcs az azonosító, az érték pedig a név.
    GUI gui;                

    public ChatClient() throws IOException {
        // A socket egy csatorna, melynek action üzeneteket küldhetünk a szervernek, és fogadhatjuk a szerver válaszát
        Socket socket = new Socket(HOSTNAME, 8082); // A 8082-es porton csatlakozunk a szerver action csatornájára
        out = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
        InputStreamReader in = new InputStreamReader(socket.getInputStream(), "UTF-8");
        actionParser = new JsonStreamParser(in);
        login();
    }
    
    /*
     * A listen() függvénnyel tudunk "hallgatózni" az event csatornán.
     * Van benne egy végtelen ciklus, amely addig fut, míg be nem
     * zárjuk a programot. A ciklusban várjuk a szerverről érkező
     * üzeneteket.
     */
    void listen() throws IOException {
        Socket eventSocket = new Socket(HOSTNAME, 8083); // A 8083-as porton hallgatózunk
        Reader eventIn = new InputStreamReader(eventSocket.getInputStream(), "UTF-8");
        JsonStreamParser eventParser = new JsonStreamParser(eventIn); // Az event csatorna JsonStreamParser-je
        while (true) {
            JsonObject obj = eventParser.next().getAsJsonObject(); // Itt meg fog állni a program, egészen addig, míg kapunk egy eseményt a szervertől.
            System.out.println(obj);
            
            String type = obj.get("eventtype").getAsString(); // kilvassuk az esemény típusát az eventtype mezőből
            if (type.equals("newmessage")) { // ha felhasználó által  küldött üzenet érkezik
                handleNewMessage(obj);
            } else if (type.equals("onlineuserschanged")) { // ha valaki kilép vagy belép, tehát változik az online felhasználók listája
                getOnlineUsers();
            } else if (type.equals("shakewindow")) {
                handleShakeWindow(); // ha shake window esemény érkezik
            }
        }
    }
    
    // Bejelentkezés action
    public void login() throws IOException {
        out.append("{ \"action\" : \"login\", \"name\" : \"" + NICKNAME + "\" }"); // bejelentkező üzenet küldése a névvel együtt
        out.flush(); // a flush() jelzi, hogy vége az üzenetnek, és el kell küldeni a szervernek

        // A szerver válaszolni fog, hogy sikeres volt a bejelentkezés, és elküldi az azonosítónkat.
        JsonElement element = actionParser.next(); // a válasz JSON element lekérése
        id = element.getAsJsonObject().get("id").getAsInt(); // az id mezőt kiolvassuk a JSON objektumból, és beállítjuk azonosítóként
    }
    
    // Üzenet küldés action
    public void sendMessage(String msg) throws IOException {
        out.append("{ \"id\" : \"" + id + "\", "
                + "\"action\" : \"send\", "
                + "\"msgid\" : \"" + msgid + "\", "
                + "\"message\" : \"" + msg + "\" "
                + "}");
        out.flush();
        msgid += 1;
        actionParser.next(); // A szerver válaszüzenetét beolvassuk
    }

    // Bérkező üzenet event kezelése
    private void handleNewMessage(JsonObject obj) {
        String msg = obj.get("message").getAsString(); // Az üzenet az objektum message mezője
        int senderId = obj.get("senderid").getAsInt(); // A feladó azonosítója pedig az id mező
        String name = onlineUsers.get(senderId);       // Lekérdezzük a feladó felhasználónevét a HashMap-ból
        gui.newMessageReceived(name, msg);             // A GUI-nak jelezzük, hogy új üzenet érkezett
    }
    
    // Online felhasználók lekérdezés action küldése
    void getOnlineUsers() throws IOException {
        out.append("{ \"id\" : " + id + ", \"action\" : \"onlineusers\" }"); // lekérdezés
        out.flush();
        
        JsonElement element = actionParser.next();  // a szerver válasza
        JsonObject object = element.getAsJsonObject();
        if (object.get("success").getAsBoolean()) { // ha sikeres, feldolgozzuk a választ
            updateOnlineUsers(object);
        }
    }
    
    /*
     * Az online felhasználók frissítése a szerver válasza alapján
    
     * A felhasználókat egy HashMap adatformátumban tároljuk, amely
     * kulcs-érték párok tárolását teszi lehetővé. Itt a kulcs az
     * azonosító, az érték pedig a felhasználónév. Tehát az azonosító
     * alapján ki tudjuk olvasni a hozzá tartozó felhasználónevet.
     *
     * Bővebben: https://docs.oracle.com/javase/9/docs/api/java/util/HashMap.html
     */
    private void updateOnlineUsers(JsonObject obj) {
        onlineUsers.clear(); // Töröljük a HashMap tartalmát
        for (JsonElement element : obj.get("onlineusers").getAsJsonArray()) { // az szerver válaszüzenetének az onlineusers mezőjét JsonElement tömbként olvassuk be, és végigmegyünk a tartalmán.
            JsonObject user = element.getAsJsonObject();  // Egy felhasználó saját JSON objektuma. Ez az objektum egy "részobjektuma" lesz az obj objektumnak, és csak egy felhasználó adatait tartalmazza.
            int id = user.get("id").getAsInt();           // Az id mező lesz a felhasználó azonosítója
            String name = user.get("name").getAsString(); // A name mező pedig a felhasználóneve
            onlineUsers.put(id, name);                    // Mentsük el a felhasználót a HashMap-be, az id kulccsal és a name értékkel
        }
        gui.onlineUsersChanged(onlineUsers.values()); // A GUI-nak elküldjük az új felhasználók listáját. A values() a HashMap értékeit adja vissza egy adathalmazként.
    }
    
    // A szervertől kapott shake window event kezelése
    void handleShakeWindow() throws IOException {
       gui.shakeWindow();
    }

    // A shake window action elküldése a szervernek
    void sendShakeWindow() throws IOException {
        out.append("{ \"id\" : " + id + ", \"action\" : \"shakewindow\" }");
        out.flush();
    }
}
